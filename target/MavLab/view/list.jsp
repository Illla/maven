<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <title>Document</title>
</head>
<body>
<div>
    <h2>Users</h2>

    <form METHOD="post">
        <div class="form-group">
            <label>Search</label>
            <input name="search" type="login" class="form-control" placeholder="Enter searching string">
        </div>
        <button type="submit" class="btn btn-primary">Search user</button>
    </form>
    <button type="submit" class="btn btn-primary" onclick="location.href='/list'">Clear</button>
    <table class="table">
        <tr>
            <th scope="col">id</th>
            <th scope="col">Абонент</th>
            <th scope="col">Номер</th>
        </tr>
        <%
            int i = 0;
            String[] arraySearch = new String[3];
            String[] array = new String[3];
            List<String> subs = (List<String>) request.getAttribute("userNames");
            List<String> subSearch = (List<String>) request.getAttribute("searchRes");
                if (subSearch != null && !subSearch.isEmpty()) {
                    for (String str : subSearch) {
                        arraySearch[i] = str;
                        i+=1;
                        if (i%3==0){
                            out.print("<tr><td>"+arraySearch[0]+"</td>" +
                                    "<td>"+arraySearch[1]+"</td>" +
                                    "<td>"+arraySearch[2]+"</td>" +
                                    "</tr>");
                            i=0;
                        }
                    }
                }
            if (subs != null && !subs.isEmpty()) {
        for (String s : subs) {
            array[i] = s;
            i+=1;
            if (i%3==0){
                out.print("<tr><td>"+array[0]+"</td>" +
                        "<td>"+array[1]+"</td>" +
                        "<td>"+array[2]+"</td>" +
                        "</tr>");
                i=0;
            }
        }
    }
%>
    </table>
    <button type="submit" class="btn btn-primary" onclick="location.href='/add'">Add user</button>
    <button type="submit" class="btn btn-primary" onclick="location.href='/del'">Delete user</button>
    <button type="submit" class="btn btn-primary" onclick="location.href='/upd'">Update user</button>
</div>
</body>
</html>