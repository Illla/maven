package app.servlets;
import app.resources.dbconnect;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class delSrv extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("view/del.jsp");
        requestDispatcher.forward(req, resp);
    }
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String idSub = req.getParameter("idSub");
        dbconnect db = new dbconnect();
        String data = db.deleteSub(idSub);
        if (data == "Successful"){
            resp.sendRedirect("/list");
        } else{
            resp.sendRedirect("/del");
        }
    }
}