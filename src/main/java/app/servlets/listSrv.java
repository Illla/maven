package app.servlets;
import app.resources.dbconnect;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class listSrv extends HttpServlet {

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<String> PhoneBook = new ArrayList();
        dbconnect db = new dbconnect();
        for (String ResPhoneArray: db.setList())
            PhoneBook.add(ResPhoneArray);
        req.setAttribute("userNames", PhoneBook);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("view/list.jsp");
        requestDispatcher.forward(req, resp);
    }
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<String> SearchResult = new ArrayList();
        dbconnect db = new dbconnect();
        String searchString = req.getParameter("search");
        for (String ResSearchArray: db.searchUser(searchString))
            SearchResult.add(ResSearchArray);
        req.setAttribute("searchRes", SearchResult);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("view/list.jsp");
        requestDispatcher.forward(req, resp);
    }
}