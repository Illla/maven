package app.servlets;
import app.resources.dbconnect;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class addSrv extends HttpServlet {
        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("view/add.jsp");
                requestDispatcher.forward(req, resp);
        }
        protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
                req.setCharacterEncoding("utf8");
                String sub = req.getParameter("sub");
                String number = req.getParameter("number");
                dbconnect db = new dbconnect();
                String data = db.createSub(sub,number);
                if (data == "Successful"){
                        resp.sendRedirect("/list");
                } else {
                        resp.sendRedirect("/add");
                }
        }
}
