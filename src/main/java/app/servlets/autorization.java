package app.servlets;
import app.resources.dbconnect;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;;

public class autorization extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("view/autorization.jsp");
        requestDispatcher.forward(req, resp);
    }
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("view/autorization.jsp");
        String log = req.getParameter("login");
        String pass = req.getParameter("pass");
        String IncAuthTemplate = "Authorization incorrected";
        dbconnect db = new dbconnect();
        String data = db.authorizationUser(log,pass);
        if (data == "Successful"){
            resp.sendRedirect("/list");
        }
        else{
            req.setAttribute("incauthtemp",IncAuthTemplate);
            requestDispatcher.forward(req, resp);
            resp.sendRedirect("/autorization");
        }
    }
}

