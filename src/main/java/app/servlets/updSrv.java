package app.servlets;
import app.resources.dbconnect;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class updSrv extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("view/upd.jsp");
        requestDispatcher.forward(req, resp);
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("view/upd.jsp");
        List<String> RSId = new ArrayList();
        dbconnect db = new dbconnect();
        String reqUpdRes = null;
        String subId = req.getParameter("subId");
        String sub = req.getParameter("sub");
        String subNum = req.getParameter("num");
        if (!subId.equals("null") && (sub.equals("null") && subNum.equals("null"))) {
            for (String ResSearchArray : db.searchId(subId))
                RSId.add(ResSearchArray);
            if (RSId.size()==0) {
                requestDispatcher.forward(req, resp);
                resp.sendRedirect("/upd");
            }
            req.setAttribute("id", RSId.get(0));
            req.setAttribute("sub", RSId.get(1));
            req.setAttribute("num", RSId.get(2));
            requestDispatcher.forward(req, resp);
        } else if (!sub.equals("null") && !subNum.equals("null")) reqUpdRes = db.updateUser(subId, sub, subNum);

        if (reqUpdRes == "Successful") {
            resp.sendRedirect("/list");
        } else {
            resp.sendRedirect("/upd");
        }
    }
}
