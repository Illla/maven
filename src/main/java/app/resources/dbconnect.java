package app.resources;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class dbconnect {
    private static Connection conndb() {
        Connection conn = null;
        try {
            String url = "jdbc:mysql://localhost/users?serverTimezone=Europe/Moscow&useSSL=false";
            String username = "root";
            String password = "123456789";
            Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
            conn = DriverManager.getConnection(url, username, password);
        } catch (Exception ex) {
            System.out.println("Connection failed...");
            System.out.println(ex);
        }
        return conn;
    }

    public String authorizationUser(String login1, String pass1) {
        Connection connect = this.conndb();
        String[] arraySearch = new String[2];
        try {
            String request = "Select login,pass from datausers where login='"+login1+"' and pass='"+pass1+"'";
            Statement statement = connect.createStatement();
            ResultSet resLogPass = statement.executeQuery(request);
            int columns = resLogPass.getMetaData().getColumnCount();
            while (resLogPass.next()) {
                for (int i = 1; i <= columns; i++) {
                    arraySearch[i-1] = resLogPass.getString(i);
                }
            }
            if (!arraySearch[0].equals(login1) && !arraySearch[1].equals(pass1)) return "failed connection";
            connect.close();
            } catch(Exception ex){
                System.out.println(ex);
                return "failed connection";
            }
            return "Successful";
    }

    public List<String> searchUser(String stringSearch) {
            List<String> ResSearchBook = new ArrayList();
            Connection connect = this.conndb();
            try {
                String request = "SELECT * FROM phonebook WHERE id LIKE '%"+stringSearch+"%'|| subscriber LIKE '%"+stringSearch+"%'|| phone LIKE '%"+stringSearch+"%'";
                Statement statement = connect.createStatement();
                ResultSet ResQuery = statement.executeQuery(request);
                int columns = ResQuery.getMetaData().getColumnCount();
                while (ResQuery.next()) {
                    for (int i = 1; i <= columns; i++) {
                        ResSearchBook.add(ResQuery.getString(i));
                    }
                }
                connect.close();
            } catch (Exception ex) {
                System.out.println(ex);
                System.out.println ("Failed");
            }
            return ResSearchBook;
    }


    public String deleteSub(String id) {
        Connection connect = this.conndb();
        try {
            String request = "DELETE from phonebook where Id = '"+id+"'";
            Statement statement = connect.createStatement();
            statement.executeUpdate(request);
            connect.close();
        } catch (Exception ex) {
            System.out.println(ex);
            System.out.println ("Failed");
        }
        return "Successful";
    }

    public String createSub(String sub, String phonesub) {
        Connection connect = this.conndb();
        try {
            String request = "Insert into phonebook (subscriber, phone) values ('"+sub+"','"+phonesub+"')";
            Statement statement = connect.createStatement();
            statement.executeUpdate(request);
            connect.close();
        } catch (Exception ex) {
            System.out.println(ex);
            System.out.println ("Failed");
        }
        return "Successful";
    }

    public List<String> setList () {
        List<String> ResPhoneBook = new ArrayList();
        Connection connect = this.conndb();
        try {
            String request = "Select * from phonebook";
            Statement statement = connect.createStatement();
            ResultSet ResQuery = statement.executeQuery(request);
            int columns = ResQuery.getMetaData().getColumnCount();
            while (ResQuery.next()) {
                for (int i = 1; i <= columns; i++) {
                    ResPhoneBook.add(ResQuery.getString(i));
                }
            }
            connect.close();
        } catch (Exception ex) {
            System.out.println(ex);
            System.out.println ("Failed");
        }
        return ResPhoneBook;
    }

   public List<String> searchId(String id) {
        List<String> ResSearchId = new ArrayList();
        Connection connect = this.conndb();
        try {
            String request = "SELECT * FROM phonebook WHERE id ='"+id+"'";
            Statement statement = connect.createStatement();
            ResultSet ResQuery = statement.executeQuery(request);
            int columns = ResQuery.getMetaData().getColumnCount();
            while (ResQuery.next()) {
                for (int i = 1; i <= columns; i++) {
                    ResSearchId.add(ResQuery.getString(i));
                }
            }
            connect.close();
        } catch (Exception ex) {
            System.out.println(ex);
            System.out.println ("Failed");
        }
        return ResSearchId;
    }

    public String updateUser(String id, String updSub, String updNum) {
        Connection connect = this.conndb();
        try {
            String request = "Update phonebook set subscriber='"+updSub+"', phone='"+updNum+"' where id='"+id+"'";
            Statement statement = connect.createStatement();
            statement.executeUpdate(request);
            connect.close();
        } catch(Exception ex){
            System.out.println(ex);
            return "failed connection";
        }
        return "Successful";
    }

}