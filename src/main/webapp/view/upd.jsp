<%@ page import="javax.naming.directory.SearchResult" %><%--
  Created by IntelliJ IDEA.
  User: Kirl
  Date: 29.05.2020
  Time: 8:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <title>Document</title>
</head>
<body>
<form METHOD="post">
<div class="form-group">
    <label>Sub id</label>
    <input name ="subId" type="login" class="form-control" placeholder="Enter subscriber id" value="<%=request.getAttribute("id")%>">
</div>
    <button type="submit" class="btn btn-primary">Search</button>
    <div class="form-group">
        <label>Edited Subscriber</label>
        <input name ="sub" type="login" class="form-control" value="<%=request.getAttribute("sub")%>">
    </div>
    <div class="form-group">
        <label>Edited Number</label>
        <input name="num" type="login" class="form-control" value="<%=request.getAttribute("num")%>">
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
</form>
<button class="btn btn-primary" onclick="location.href='/list'">Back to list</button>
</body>
</html>
